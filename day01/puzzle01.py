file = open('./input01.txt')
lines = file.read().splitlines()

for i, first in enumerate(lines):
    for j, second in enumerate(lines):
        for k, third in enumerate(lines):
            if i != j and j != k and i != k:
                if (int(first) + int(second) + int(third)) == 2020:
                    print("Found indexes:", i, j, k)
                    print("Values:", int(first), int(second), int(third))
                    print(int(first) * int(second) * int(third))
                    exit()

print ("hmmm")