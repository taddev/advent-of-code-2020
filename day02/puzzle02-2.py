file = open('./input02.txt')
lines = file.read().splitlines()

valid = 0

for i, line in enumerate(lines):
    data = line.split()
    bounds = data[0].split('-')
    policy = data[1].split(':')[0]
    password = list(data[2])

    one = int(bounds[0]) - 1
    two = int(bounds[1]) - 1

    if password[one] == policy and password[two] != policy:
        valid = valid+1
        print('Valid')
    elif password[two] == policy and password[one] != policy:
        valid = valid + 1
        print('Valid')
    else:
        print('Not Valid')


print("Total valid:", valid)