file = open('./input02.txt')
lines = file.read().splitlines()

valid = 0

for i, line in enumerate(lines):
    data = line.split()
    bounds = data[0].split('-')
    policy = data[1].split(':')[0]
    password = data[2]

    count = 0
    for i in password:
        if i == policy:
            count = count+1

    if count >= int(bounds[0]) and count <= int(bounds[1]):
        valid = valid + 1
        print('valid')
    else:
        print('not valid')


print("Total valid:", valid)